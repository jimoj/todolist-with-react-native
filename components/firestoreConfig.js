
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAN-dkCeSR4wI4GW1qlqWGDMpoSD4KvuIc",
  authDomain: "todo-list-with-react-native.firebaseapp.com",
  projectId: "todo-list-with-react-native",
  storageBucket: "todo-list-with-react-native.appspot.com",
  messagingSenderId: "464014789780",
  appId: "1:464014789780:web:d35e9472da622451127881",
  measurementId: "G-GK2CCQ1CT1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export default app;