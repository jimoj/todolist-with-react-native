import react from "react";
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import { KeyboardAvoidingView, TextInput } from "react-native";
import { getFirestore, collection, getDocs, addDoc, deleteDoc, doc } from 'firebase/firestore/lite';



const Task = (props) => {





    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
            <TouchableOpacity style={styles.square}>
            </TouchableOpacity>
                <Text style={styles.text}>
                    {props.text}
                </Text>
            </View>
            <View style={styles.circular}></View>


            



        </View>




    )
}       

    const styles = StyleSheet.create({
        item: {
            backgroundColor:'#FFF',
            padding:15,
            borderRadius: 10,
            flexDirection: 'row',
            alignItems:'center',
            justifyContent: 'space-between',
            marginBottom: 20,
            
        },
        itemLeft: {
            flexDirection: 'row',
            alignItems: 'center',
            flexWrap:'wrap'
        },
        square: {
            width:24,
            height: 24,
            backgroundColor: '#55BCF6',
            opacity: 0.4,
            borderRadius:5,
            marginRight:15
        },
        itemText: {},
        circular: {
            width: 12,
            height: 12,
            borderWidth:2,
            borderColor:'#55BCF6',
            borderRadius: 5,
        },
        
    });


export default Task;