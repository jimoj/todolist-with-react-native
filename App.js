import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Task from './components/Task';
import { KeyboardAvoidingView, TextInput, TouchableOpacity, Platform, Keyboard } from "react-native";
import app from './components/firestoreConfig';
import { getFirestore, collection, getDocs, addDoc, deleteDoc, doc, query, where } from 'firebase/firestore/lite';
import { useEffect, useState } from 'react';
import { Fragment } from 'react/cjs/react.production.min';

export default function App() {


  const db = getFirestore(app);
  const [listTask, setListTask] = useState([])
  const [newTask,setNewTask] = useState();


    //let {setTasks} = props;
    const handleDelete = async (idToDelete) => {
      let copyListTask = [...listTask];
      let filteredTasks = copyListTask.filter(({nombre}) => {
           return nombre !== idToDelete;
      });
      setListTask(filteredTasks);
      //await deleteDoc(doc(db, "tasks", idToDelete));
      const q = query(collection(db,"tasks"), where("nombre", "==", idToDelete))
      let docToDelete = await getDocs(q);
      docToDelete.forEach((document) => {
         deleteDoc(doc(db, "tasks", document.id));
      })
  
  
  
    }

  const handleAddTask =  async () => {
    Keyboard.dismiss(); //esto sirve para ocultar el keyboard que sale en pantala
    let nuevaTarea = {
      nombre: newTask,
      descripcion: "prueba añadir a firestore"
    }
    let taskRef;
    try {
      taskRef = await addDoc(collection(db, 'tasks'), nuevaTarea);

    } catch (e) {
      console.log("Promise addDoc not working")
    }
    nuevaTarea.id = taskRef.id;
    setListTask([...listTask, nuevaTarea]);
  }

  async function getTasks (db) {
    const taskCollection = collection(db, 'tasks');
    const taskSnapshot = await getDocs(taskCollection);
    const taskList = taskSnapshot.docs.map(doc => doc.data());
    return taskList;
  }


  useEffect(() => {
    getTasks(db).then(
      json => {
        let arrayDocuments = [];
        //const parsedJson = JSON.parse(json);
        json.map(task => {
          arrayDocuments = [...arrayDocuments, task]
      })
      setListTask(arrayDocuments);
    })
    .catch(error => {
      console.log("Ha ocurrido un error al intentar acceder a firestore")
    })
  }, []);

  
  return (
    <View style={styles.container}>
      {/* Lista de tareas */}
      <View style={styles.taskWrapper}>
        <Text style={styles.sectionTitle}> Lista modificable</Text>
        
        <View style={styles.items}>
          {/* Aqui van a ir las tareas */}
          
          {listTask.map((n, index) => {
            return (
            <TouchableOpacity onPress={() => {handleDelete(n.nombre)}}>
                <Task key={index} text={n.nombre}/>
            </TouchableOpacity>
            
              
              
              )
          })}

          
        </View>
      </View>
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={styles.writeTaskWrapper}>
                
                <TextInput style={styles.input} placeholder={"Escribe una tarea"} onChangeText={text => {setNewTask(text)}}/>
                <TouchableOpacity onPress={handleAddTask}>
                    <View style={styles.addWrapper}>
                        <Text style={styles.addText}>
                            +
                        </Text>
                    </View>
                </TouchableOpacity>
            
            </KeyboardAvoidingView>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
    
  },
  taskWrapper: {
    paddingTop: 80,
    paddingHorizontal:20
  },
  sectionTitle: {
    fontSize:24,
    fontWeight:'bold'
  },
  items: {
    marginTop: 30,
  },
  writeTaskWrapper: {
    position: 'absolute',
    bottom: 60,
    width:'100%',
    flexDirection: 'row',
    justifyContent:'space-around',
    alignItems:'center'

},
input: {
    paddingVertical:15,
    paddingHorizontal:15,
    width: 250,
    backgroundColor: '#FFF',
    borderRadius: 60,
    borderColor:'#C0C0C0',
    borderWidth: 1,
    
},
addWrapper: {
    width:60,
    height:60,
    backgroundColor:'#FFF',
    borderRadius:60,
    justifyContent:'center',
    alignItems:'center',
    borderColor:'#C0C0C0',
    borderWidth: 1,

},
addText: {}
});
